# -*- coding: utf-8 -*-
"""
Created on Wed May  3 12:20:47 2023

@author: aseem
"""

import numpy as np
import matplotlib.pyplot as plt

from airgapmmf import Single_layer_Winding

class Double_Layer_Winding(Single_layer_Winding):
    
    def __init__(self, num_slots, num_poles, num_phases, current_layer1 = 1, current_layer2 = 1, shortening=0):
        
        self.num_layers = 2
        self.current_layer1 = current_layer1
        self.current_layer2 = current_layer2
        self.shortening = 0
        super().__init__(num_slots, num_poles, num_phases, self.num_layers, current_layer1, 0)
        # Generate waveforms for different numbers of stator slots, poles, phases, and layers
        waveformA_M1, waveformB_M1, waveformC_M1, airgap_mmf_M1, slot_angles_M1 = self.generate_mmfs()
        airgap_mmf = airgap_mmf_M1
        
        # Plot the phase mmf waveforms for winding layer 1
        fig1,ax1 = plt.subplots()
        
        ax1.step(slot_angles_M1,waveformA_M1, label='Phase A')
        ax1.step(slot_angles_M1,waveformB_M1,label='Phase B')
        ax1.step(slot_angles_M1,waveformC_M1,label='Phase C')
        ax1.legend(loc='best')
        ax1.set_xlabel('Electrical Angle (Degrees)')
        ax1.set_ylabel('MMF Amplitude')
        ax1.axhline(y=0,color='r',linestyle='dashed',linewidth=1)
        ax1.set_xlim(0,360)
        ax1.set_xticks(np.linspace(0,360,9))    
        ax1.set_title('Phase MMF Waveforms for Winding Layer 1')
        
        # Plot the airgap mmf waveform for winding layer 1 ( Zero shortening)
        fig2,ax2 = plt.subplots()
        
        ax2.step(slot_angles_M1,airgap_mmf_M1, label='AirGap MMF 1')
        ax2.set_title('Airgap MMF Waveform for Winding Layer 1')
        ax2.set_xlabel('Electrical Angle (Degrees)')
        ax2.set_ylabel('MMF Amplitude')
        ax2.legend(loc='best')
        ax2.set_xlim(0,360)
        ax2.set_xticks(np.linspace(0,360,9))
        
        super().__init__(num_slots, num_poles, num_phases, self.num_layers, current_layer2, shortening)        
        self.shortening = shortening
         
        
    
        #Machine_M2 =  Single_layer_Winding(num_slots, num_poles, num_phases, num_layers, self.current_layer2,shortening)
        
        # Generate waveforms for different numbers of stator slots, poles, phases, and layers
        waveformA_M2, waveformB_M2, waveformC_M2, airgap_mmf_M2, slot_angles_M2 = self.generate_mmfs()
        airgap_mmf = airgap_mmf + airgap_mmf_M2

        fig3,ax3 = plt.subplots()
        ax3.set_title('Phase MMF Waveforms for Winding 2 with shortening = '+ str(shortening)+ " stator slots")
        ax3.step(slot_angles_M2,waveformA_M2, label='Phase A')
        ax3.step(slot_angles_M2,waveformB_M2,label='Phase B')
        ax3.step(slot_angles_M2,waveformC_M2,label='Phase C')
        ax3.axhline(y=0,color='r',linestyle='dashed',linewidth=1)
        ax3.set_xlabel('Electrical Angle (Degrees)')
        ax3.set_ylabel('MMF Amplitude')
        ax3.legend(loc='best')
        ax3.set_xlim(0,360)
        ax3.set_xticks(np.linspace(0,360,9))
    

        fig4,ax4 = plt.subplots()    
        ax4.step(slot_angles_M2,airgap_mmf_M2, label='AirGap MMF 2')
        ax4.set_title('Airgap MMF Waveform, for 2nd winding layer, shortening = '+ str(shortening)+ " stator slots")
        ax4.set_xlabel('Electrical Angle (Degrees)')
        ax4.set_ylabel('MMF Amplitude')
        ax4.legend(loc='best')
        ax4.set_xlim(0,360)
        ax4.set_xticks(np.linspace(0,360,9)) 
        
        fig5,ax5 = plt.subplots()    
        ax5.step(slot_angles_M2,airgap_mmf, label='AirGap MMF Combined')
        ax5.set_title('Airgap MMF Waveform, Two Layers Combined')
        ax5.set_xlabel('Electrical Angle (Degrees)')
        ax5.set_ylabel('MMF Amplitude')
        ax5.legend(loc='best')
        ax5.set_xlim(0,360)
        ax5.set_xticks(np.linspace(0,360,9)) 
        
        plt.show()

if __name__ == '__main__':
    
    num_slots=36
    num_poles=4
    num_phases=3
    num_layers=2
    shortening = 6
    current_layer1 = 1
    current_layer2 = 1
    
    Machine_M1 = Double_Layer_Winding(num_slots, num_poles, num_phases, current_layer1, current_layer2, shortening)