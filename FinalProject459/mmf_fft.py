import numpy as np
import matplotlib.pyplot as plt
from airgapmmf import Single_layer_Winding

class Airgap_MMF_FFT(Single_layer_Winding):
    
    def __init__(self,num_slots=36, num_poles=4, num_phases=3, num_layers=1,current=1,shortening = 0):
        super().__init__(num_slots, num_poles, num_phases, num_layers, current, shortening)
        self.generate_mmfs()
        self.signal = np.zeros(self.num_slots*2)
        self.signal[::2] = self.airgap_mmf[:-1]
        
        
    def get_fft(self):
        self.fft = np.fft.fft(self.signal)
        self.freqs = np.fft.fftfreq(self.num_slots * 2, d=1/self.num_slots)
        self.amps = np.abs(self.fft) /self.num_slots # amplitudes of frequencies
        
    def get_fft_variable_window(self,start_index,window_length):
        self.signal_var = self.signal[start_index:(start_index+window_length)]
        self.fft = np.fft.fft(self.signal_var)
        self.freqs = np.fft.fftfreq(self.num_slots * 2, d=1/self.num_slots)
        self.amps = np.abs(self.fft) /self.num_slots # amplitudes of frequencies
     
    def plot_fft(self):
        n = self.num_slots
        fig, (ax1, ax2) = plt.subplots(nrows=2, ncols=1, sharex=True, figsize=(8, 6))

        # Plot the original signal
        ax1.step(np.arange(self.num_slots), self.airgap_mmf[:-1])# linefmt='b-', basefmt=' ', markerfmt='bo', use_line_collection=True)
        ax1.set_ylabel('Amplitude')
        ax1.set_title('Original Signal')

        # Plot the FFT
        ax2.stem(self.freqs[:n // 2], self.amps[:n // 2], linefmt='b-', basefmt=' ', markerfmt='bo', use_line_collection=True)
        ax2.set_xlabel('Frequency')
        ax2.set_ylabel('Amplitude')
        ax2.set_title('Fast Fourier Transform')

        plt.tight_layout()
        plt.show()
            

if __name__ == '__main__':
    
    num_slots=36
    num_poles=4
    num_phases=3
    num_layers=2
    shortening = 2
    current_layer1 = 1
    current_layer2 = 1
    
    Machine_M1 = Airgap_MMF_FFT(num_slots, num_poles, num_phases, num_layers,current_layer1)
    
    # Generate waveforms for different numbers of stator slots, poles, phases, and layers
    waveformA_M1, waveformB_M1, waveformC_M1, airgap_mmf_M1, slot_angles_M1 = Machine_M1.generate_mmfs()
    airgap_mmf = airgap_mmf_M1
    
    #fft_obj = Airgap_MMF_FFT()
    Machine_M1.get_fft()
    Machine_M1.plot_fft()
    
    # Machine_M1.get_fft_variable_window(4, 18)
    # Machine_M1.plot_fft()