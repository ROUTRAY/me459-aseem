# -*- coding: utf-8 -*-
"""
Created on Tue May  2 23:48:49 2023

@author: aseem
"""

import numpy as np
import matplotlib.pyplot as plt

class Single_layer_Winding:
    
    def __init__(self,num_slots=36, num_poles=4, num_phases=3, num_layers=1,current=1,shortening = 0):
        self.num_slots = num_slots
        self.num_poles = num_poles
        self.num_phases = num_phases
        self.num_layers = num_layers
        self.shortening = shortening
        self.current = current

    def generate_mmfs(self):
    
        # Calculate the number of slots per pole per phase
        #pitch = 1 # always for a single layer winding
        #pitch = (q-shortening) / q # may not remain a func of q for double layer
        q = (int)(self.num_slots /(self.num_poles * self.num_phases)) # slots/pole/phase
    
        
        # Calculate the angle between two adjacent stator slots
        slot_angle = np.degrees(2 * np.pi / self.num_slots)
        #opening_angle = slot_angle*self.slot_opening_ratio
        
        slots_per_pole = (int)(self.num_slots/self.num_poles)
        
        # Calculate the angle for each slot center
        self.slot_angles = np.arange(self.num_slots) * slot_angle#+ slot_angle / 2
        
        # Generate the waveform        
        coil_directions_A = np.zeros(self.num_slots) 
        pattern = np.zeros(2*slots_per_pole)
        #indices = np.arange(self.num_slots)
        pattern[:q]=1
        pattern[slots_per_pole:slots_per_pole+q]=-1
        coil_directions_A = np.tile(pattern, int(np.ceil(self.num_poles/2)))

        
        def shift_array_by_index(arr, shift):
            shifted_arr = np.roll(arr.copy(), shift)  # create a copy and shift it
            return shifted_arr
        
        coil_directions_A = shift_array_by_index(coil_directions_A.copy(),self.shortening)
        coil_directions_B = shift_array_by_index(coil_directions_A.copy(),-1*int(2*q))
        coil_directions_C = shift_array_by_index(coil_directions_A.copy(),int(2*q))
        
        # if self.shortening==0:
        #     print(coil_directions_A)
        #     print(coil_directions_B)
        #     print(coil_directions_C)
        #     print("\n")
               
        self.coil_directions_A = np.insert(coil_directions_A,len(self.slot_angles),coil_directions_A[0])
        self.coil_directions_B = np.insert(coil_directions_B,len(self.slot_angles),coil_directions_B[0])
        self.coil_directions_C = np.insert(coil_directions_C,len(self.slot_angles),coil_directions_C[0])
        
        # if self.shortening==0:
        #     print(self.coil_directions_A)
        #     print(self.coil_directions_B)
        #     print(self.coil_directions_C)
        
        self.slot_angles = np.insert(self.slot_angles,len(self.slot_angles),360)
        
        self.countingfunc_A = np.cumsum(self.coil_directions_A)
        self.countingfunc_B = np.cumsum(self.coil_directions_B)
        self.countingfunc_C = np.cumsum(self.coil_directions_C)
        
        self.windingfunc_A = self.countingfunc_A-round(np.mean(self.countingfunc_A),1)
        self.windingfunc_B = self.countingfunc_B-round(np.mean(self.countingfunc_B),1)
        self.windingfunc_C = self.countingfunc_C-round(np.mean(self.countingfunc_C),1)
        
        self.mmfphase_A = self.current*self.windingfunc_A
        self.mmfphase_B = self.current*self.windingfunc_B
        self.mmfphase_C = self.current*self.windingfunc_C
        
        
        self.airgap_mmf = self.mmfphase_A + self.mmfphase_B*np.cos(2*np.pi/3)+ self.mmfphase_C*np.cos(2*np.pi/3)

    
        return self.mmfphase_A, self.mmfphase_B, self.mmfphase_C,  self.airgap_mmf, self.slot_angles


if __name__ == '__main__':
    
    num_slots=36
    num_poles=4
    num_phases=3
    num_layers=2
    shortening = 4
    current_layer1 = 1
    current_layer2 = 1
    
    Machine_M1 = Single_layer_Winding(num_slots, num_poles, num_phases, num_layers,current_layer1)
    
    # Generate waveforms for different numbers of stator slots, poles, phases, and layers
    waveformA_M1, waveformB_M1, waveformC_M1, airgap_mmf_M1, slot_angles_M1 = Machine_M1.generate_mmfs()
    airgap_mmf = airgap_mmf_M1
    
     
    # Plot the phase mmf waveforms for winding layer 1
    fig1,ax1 = plt.subplots()
    
    ax1.step(slot_angles_M1,waveformA_M1, label='Phase A')
    ax1.step(slot_angles_M1,waveformB_M1,label='Phase B')
    ax1.step(slot_angles_M1,waveformC_M1,label='Phase C')
    ax1.legend(loc='best')
    ax1.set_xlabel('Electrical Angle (Degrees)')
    ax1.set_ylabel('MMF Amplitude')
    ax1.axhline(y=0,color='r',linestyle='dashed',linewidth=1)
    ax1.set_xlim(0,360)
    ax1.set_xticks(np.linspace(0,360,9))    
    ax1.set_title('Phase MMF Waveforms for Winding Layer 1')
    
    # Plot the airgap mmf waveform for winding layer 1 ( Zero shortening)
    fig2,ax2 = plt.subplots()
    
    ax2.step(slot_angles_M1,airgap_mmf_M1, label='AirGap MMF 1')
    ax2.set_title('Airgap MMF Waveform for Winding Layer 1')
    ax2.set_xlabel('Electrical Angle (Degrees)')
    ax2.set_ylabel('MMF Amplitude')
    ax2.legend(loc='best')
    ax2.set_xlim(0,360)
    ax2.set_xticks(np.linspace(0,360,9)) 
    
    
    # if num_layers==2:
    #     Machine_M2 =  Single_layer_Winding(num_slots, num_poles, num_phases, num_layers, current_layer2,shortening)
        
    #     # Generate waveforms for different numbers of stator slots, poles, phases, and layers
    #     waveformA_M2, waveformB_M2, waveformC_M2, airgap_mmf_M2, slot_angles_M2 = Machine_M2.generate_mmfs()
    #     airgap_mmf = airgap_mmf+airgap_mmf_M2

    #     fig3,ax3 = plt.subplots()
    #     ax3.set_title('Phase MMF Waveforms for Winding 2 with shortening = '+ str(shortening)+ " stator slots")
    #     ax3.step(slot_angles_M2,waveformA_M2, label='Phase A')
    #     ax3.step(slot_angles_M2,waveformB_M2,label='Phase B')
    #     ax3.step(slot_angles_M2,waveformC_M2,label='Phase C')
    #     ax3.axhline(y=0,color='r',linestyle='dashed',linewidth=1)
    #     ax3.set_xlabel('Electrical Angle (Degrees)')
    #     ax3.set_ylabel('MMF Amplitude')
    #     ax3.legend(loc='best')
    #     ax3.set_xlim(0,360)
    #     ax3.set_xticks(np.linspace(0,360,9))
    

    #     fig4,ax4 = plt.subplots()    
    #     ax4.step(slot_angles_M2,airgap_mmf_M2, label='AirGap MMF 2')
    #     ax4.set_title('Airgap MMF Waveform, for 2nd winding layer Shortened')
    #     ax4.set_xlabel('Electrical Angle (Degrees)')
    #     ax4.set_ylabel('MMF Amplitude')
    #     ax4.legend(loc='best')
    #     ax4.set_xlim(0,360)
    #     ax4.set_xticks(np.linspace(0,360,9)) 
    
    # fig5,ax5 = plt.subplots()    
    # ax5.step(slot_angles_M2,airgap_mmf, label='AirGap MMF Combined')
    # ax5.set_title('Airgap MMF Waveform, Two Layers Combined')
    # ax5.set_xlabel('Electrical Angle (Degrees)')
    # ax5.set_ylabel('MMF Amplitude')
    # ax5.legend(loc='best')
    # ax5.set_xlim(0,360)
    # ax5.set_xticks(np.linspace(0,360,9)) 
    
    plt.show()
