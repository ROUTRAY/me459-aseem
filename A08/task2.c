/* code adapted from first implementation of:
 https://www.geeksforgeeks.org/dynamically-allocate-2d-array-c/
*/
#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include "sumArray.h"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("Usage: %s <matrix dimension>\n", argv[0]);
        return 1;
    }

    size_t n = atoi(argv[1]);

    //allocate memory for Matrix A
    double *A = (double*) malloc(n*n*sizeof(double));//pointer array to store row addresses
    
    /* This code below is not going to work as the function definition only accepts single stage pointer
    This would have worked if sumArray1 could accept const double **A instead of const double *A

     for(size_t i = 0; i<n; i++) //dynamically allocate memory for each pointer
    {
        A[i] = (double*) malloc(n*sizeof(double));
    }

    */
        
    srand(time(NULL));

    for(size_t i = 0; i<(n*n); i++)
    {
            A[i] = ((double) rand() /RAND_MAX) * 2.0 - 1.0;//here 1.0 is maximum value in the range of random numbers
            // it is divided by RAND_MAX which is a constant predefined in the stdlib
    }

    double sum1 = 0.0;
    double sum2 = 0.0;
    double time1 = 0.0;
    double time2 = 0.0;
    clock_t time_start, time_end;

    time_start = clock();
    sum1 = sumArray1(A,n);
    time_end = clock();
    time1 =  ((double) (time_end - time_start) / CLOCKS_PER_SEC) * 1000;//time1 in milliseconds

    time_start = clock();
    sum2 = sumArray2(A,n);
    time_end = clock();
    time2 =  ((double) (time_end - time_start) / CLOCKS_PER_SEC) * 1000;//time2 in milliseconds

    printf("%f\n", time1);
    printf("%f\n", sum1);

    printf("%f\n", time2);
    printf("%f\n", sum2);

/*     for (size_t i = 0; i < n; i++) 
    {
        free(A[i]);
    } */

    free(A);

    return 0;

}

