#include<stdio.h>
#include "matmul.h"

void mmul1(const double* A, const double* B, double *C, const size_t n)
{
    for(size_t i=0; i<n; i++) //rows of C
    {
        for(size_t j=0; j<n; j++) //columns of C
        {
            C[i*n + j] = 0.0;
            for(size_t k=0; k<n; k++) // iterates over dot product of A and B
            {
                C[i*n + j] += A[i*n+k]*B[k*n+j]; // A and B have elements in row-major order
            }
        }
    }
}

// outer two loops swapped with respect to mmul1
void mmul2(const double* A, const double* B, double *C, const size_t n)
{
    for(size_t j=0; j<n; j++) //columns of C
    {
        for(size_t i=0; i<n; i++) //rows of C
        {
            C[i*n + j] = 0.0;
            for(size_t k=0; k<n; k++) // iterates over dot product of A and B
            {
                C[i*n + j] += A[i*n+k]*B[k*n+j]; // A and B have elements in row-major order
            }
        }
    }
}

//A is stored in row-major order and B is stored in column-major order
void mmul3(const double* A, const double* B, double *C, const size_t n)
{
    for(size_t i=0; i<n; i++) //rows of C
    {
        for(size_t j=0; j<n; j++) //columns of C
        {
            C[i*n + j] = 0.0;
            for(size_t k=0; k<n; k++) // iterates over dot product of A and B
            {
                C[i*n + j] += A[i*n+k]*B[j*n+k]; // A in row-major order and B in column major order
            }
        }
    }
}

//A is stored in column-major order and B is stored in row-major order
void mmul4(const double* A, const double* B, double *C, const size_t n)
{
    for(size_t i=0; i<n; i++) //rows of C
    {
        for(size_t j=0; j<n; j++) //columns of C
        {
            C[i*n + j] = 0.0;
            for(size_t k=0; k<n; k++) // iterates over dot product of A and B
            {
                C[i*n + j] += A[k*n+i]*B[k*n+j]; // A in column-major order and B in row-major order
            }
        }
    }
}

