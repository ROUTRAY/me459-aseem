#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include "matmul.h"

int main()
{
    size_t n = 1024;//1024
    srand(time(NULL));
    double *A = (double*) malloc(n*n*sizeof(double));
    double *B = (double*) malloc(n*n*sizeof(double));
    double *C = (double*) malloc(n*n*sizeof(double));

    double *A_column_major = (double*) malloc(n*n*sizeof(double));
    double *B_column_major = (double*) malloc(n*n*sizeof(double));

    for(size_t i = 0; i<(n*n); i++)
    {
            A[i] = ((double) rand() /RAND_MAX) * 2.0 - 1.0;//here 1.0 is maximum value in the range of random numbers
            // it is divided by RAND_MAX which is a constant predefined in the stdlib

            B[i] = ((double) rand() /RAND_MAX) * 2.0 - 1.0;
    }

    // Reordering A and B matrices for mmul3 and mmul4 matrix operations
    for(size_t i=0; i<n; i++) //rows of C
    {
        for(size_t j=0; j<n; j++) //columns of C
        {
            A_column_major[j*n + i] = A[i*n+j];
            B_column_major[j*n + i] = B[i*n+j];
        }
    }

    double time1 = 0.0;
    double time2 = 0.0;
    double time3 = 0.0;
    double time4 = 0.0;
    clock_t time_start, time_end;

    time_start = clock();
    mmul1(A,B,C,n);
    time_end = clock();
    time1 =  ((double) (time_end - time_start) / CLOCKS_PER_SEC) * 1000;//time1 in milliseconds
    printf("%f\n", time1);
    printf("%f\n", C[n*n-1]);

    time_start = clock();
    mmul2(A,B,C,n);
    time_end = clock();
    time2 =  ((double) (time_end - time_start) / CLOCKS_PER_SEC) * 1000;//time2 in milliseconds
    printf("%f\n", time2);
    printf("%f\n", C[n*n-1]);


    time_start = clock();
    mmul3(A,B_column_major,C,n);
    //mmul3(A,B,C,n);
    time_end = clock();
    time3 =  ((double) (time_end - time_start) / CLOCKS_PER_SEC) * 1000;//time3 in milliseconds
    printf("%f\n", time3);
    printf("%f\n", C[n*n-1]);


    time_start = clock();
    mmul4(A_column_major,B,C,n);
    //mmul4(A,B,C,n)
    time_end = clock();
    time4 =  ((double) (time_end - time_start) / CLOCKS_PER_SEC) * 1000;//time4 in milliseconds
    printf("%f\n", time4);
    printf("%f\n", C[n*n-1]);
  

    free(A);
    free(B);
    free(C);
    free(A_column_major);
    free(B_column_major);

    return 0;

}

