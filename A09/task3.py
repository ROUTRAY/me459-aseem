# -*- coding: utf-8 -*-
"""
Created on Wed Apr 19 17:18:54 2023

@author: aseem
"""

import numpy as np
import time

# Initialize matrices A and B with random numbers in the range of [-1, 1]
A = np.round(np.random.uniform(low=-1, high=1, size=(3400, 1400)),2)
B = np.round(np.random.uniform(low=-1, high=1, size=(3400, 1400)),2)

# Print out the last entry of A and B
print(A[-1, -1],",", B[-1, -1])

# swapping elements in both matrices row by row
tic = time.perf_counter()  # start watch
for i in range(3400):
    for j in range(1400):
        temp = A[i][j]
        A[i][j] = B[i][j]
        B[i][j] = temp
toc = time.perf_counter()  # stop watch

print(A[-1, -1],",", B[-1, -1])
print(f"{(toc - tic)*1000:0.2f}")

# swapping elements in both matrices column by column
tic = time.perf_counter()  # start watch
for j in range(1400):
    for i in range(3400):
        temp = A[i][j]
        A[i][j] = B[i][j]
        B[i][j] = temp
toc = time.perf_counter()  # stop watch

print(A[-1, -1],",", B[-1, -1])
print(f"{(toc - tic)*1000:0.2f}")
