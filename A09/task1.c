/* code adapted from first implementation of:
 https://www.geeksforgeeks.org/dynamically-allocate-2d-array-c/
*/

#include<stdio.h>
#include<stdlib.h>
#include<time.h>
#include "matmul.h"

int main(int argc, char *argv[])
{
    if (argc != 2)
    {
        printf("Usage: %s <matrix dimension>\n", argv[0]);
        return 1;
    }

    size_t n = atoi(argv[1]);

    double *A = (double*) malloc(n*n*sizeof(double));
    double *B = (double*) malloc(n*n*sizeof(double));
    double *C = (double*) malloc(n*n*sizeof(double));

    srand(time(NULL));

    for(size_t i =0; i<n*n;i++)
    {
        A[i] = ((double) rand() /RAND_MAX) * 2.0 - 1.0;
        B[i] = ((double) rand() /RAND_MAX) * 2.0 - 1.0;

    }

    double time1=0.0;
    clock_t time_start, time_end;

    time_start = clock();
    mmul1(A,B,C,n);
    time_end = clock();
    time1 =  ((double) (time_end - time_start) / CLOCKS_PER_SEC) * 1000;//time1 in milliseconds
    printf("%f\n", time1);
    printf("%f\n", C[n*n-1]);

    return 0;

    free(A);
    free(B);
    free(C);

}
