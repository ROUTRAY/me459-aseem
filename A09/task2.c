#include<stdio.h>
#include<stdlib.h>
#include<time.h>

void test1(double* data, int elems, int stride)
{
    int i;
    double result =0.0;
    volatile double sink;
    for(i=0;i<elems;i+=stride)
    {
        result+=data[i];

    }
    sink = result;
}

void test2(double* data, int elems, int stride)
{
    int i;
    volatile double result =0.0;
    volatile double sink;
    for(i=0;i<elems;i+=stride)
    {
        result+=data[i];

    }
    sink = result;
}

int main()
{
    // repeat for stride = {1,2,4,8,1,,15,17}

    int n_elems = (1024 * 512) / sizeof(double); // calculate n_elems to fill 512KB

    double data[n_elems];

    srand(time(NULL)); // seed the random number generator

    for (int i = 0; i < n_elems; i++)
    {
        data[i] = (double)rand() / RAND_MAX * 2.0 - 1.0; // generate a random double in the range [-1,1]
    }

    int strides[7] = {1,2,4,5,11,15,17};
    clock_t time_start, time_end;
    double time_strides1[7];
    double time_strides2[7];

    // function call to warm up cache
    test1(data,n_elems,strides[0]);
    for (size_t i = 0; i < sizeof(strides)/sizeof(int); i++)
    {
        time_strides1[i]=0.0;
        for (int j = 0; j < 100; j++)
        {
        time_start = clock();
        test1(data,n_elems,strides[i]);
        time_end = clock();
        time_strides1[i] +=  ((double) (time_end - time_start) / CLOCKS_PER_SEC) * 1000;//time1 in milliseconds
        }
        time_strides1[i]=time_strides1[i]/100;
        printf(" %f ",time_strides1[i]);
    }

    printf("\n");

    // function call to warm up cache
    test2(data,n_elems,strides[0]);
    for (size_t i = 0; i < sizeof(strides)/sizeof(int); i++)
    {
        time_strides2[i]=0.0;
        for (int j = 0; j < 100; j++)
        {
        time_start = clock();
        test2(data,n_elems,strides[i]);
        time_end = clock();
        time_strides2[i] +=  ((double) (time_end - time_start) / CLOCKS_PER_SEC) * 1000;//time1 in milliseconds
        }
        time_strides2[i]=time_strides2[i]/100;
        printf(" %f ",time_strides2[i]);
    }

    return 0; 
}

