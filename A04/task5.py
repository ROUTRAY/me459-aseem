# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 15:37:33 2023

@author: aseem
"""

class Student:
    
    def __init__(self,lastN='Popescu',GPA=3.8):
        self.lastName = lastN
        if GPA<=4.0 and GPA>=0:
            self.gpa = GPA
        else:
            print('WARNING: GPA value outside the range [0,4]')
    
    def compareGPA(self,StudentObj):
        if self.gpa>=StudentObj.gpa:
            print(self.lastName)
        else:
            print(StudentObj.lastName)


if __name__ == '__main__':
    S1=Student('John',3.5)
    S2=Student('Mark',3.8)
    S2.compareGPA(S1)
