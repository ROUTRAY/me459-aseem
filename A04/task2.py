def middle(a):
	print("list passed to the function: ",a)
	b=a[:] #prevents a change to the original list passed as argument
	del b[0]
	del b[-1]
	print("list returned to the function: ",b)
	return b

if __name__ == '__main__':
	A = [1,2,3,4,5]
	B = middle(A)
	print(B)
