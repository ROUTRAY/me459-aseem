# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 14:50:01 2023

@author: aseem
"""

import time
import math
from random import randint
from pySort import sorter

for i in range(3,7):
    
    n = int(math.pow(10,i))
    
    A = [randint(-1000,1000) for j in range(0,n)] #generated randomly, n x n matrix
    print('The first element of A before sorting is',A[0])
    tic = time.perf_counter()  # start watch
    sorter(A)
    toc = time.perf_counter()  # stop watch
    print(f"Elapsed time: {toc - tic:0.9f} s with perf_counter for N =", n)
    print('The first element of A after sorting is', A[0])
    print('\n')
