# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 14:30:33 2023

@author: aseem
"""


"""
code adapted from:

https://www.geeksforgeeks.org/sorting-algorithms-in-python/
"""

def sorter(A):
    
    #Insertion Sort
    for i in range(1, len(A)):
        a = A[i]
        j = i - 1
        
        while j >= 0 and a < A[j]:
            A[j + 1] = A[j]
            j -= 1
        
        A[j + 1] = a



if __name__ == '__main__':
    A =[1,0,0,1]
    sorter(A)
    print(A)
