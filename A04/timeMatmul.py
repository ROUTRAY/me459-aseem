# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 13:20:26 2023

@author: aseem
"""

import time
import math
from random import randint
from pyMatmul import matmul

for i in [7,8,9]:
    
    n = int(math.pow(2,i))
    
    A = [[randint(-100,100) for j in range(0,n)] for k in range(0,n)]# generated randomly, n x n matrix
    B = [[randint(-100,100) for j in range(0,n)] for k in range(0,n)]# generated randomly, n x n matrix
    # print(A)
    # print(B)
    tic = time.perf_counter()  # start watch
    
    C = matmul(A,B)
    
    toc = time.perf_counter()  # stop watch
    print(f"Elapsed time: {toc - tic:0.9f} s with perf_counter for N =", n)


