#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "mvmul.h"

int main(int argc, char *argv[])
{
    // assume 
    if(argc==2)
    {
        int n = atoi(argv[1]);

        clock_t start_time, end_time;
        double time_elapsed;

        srand(time(NULL));

        double *A = (double *) malloc((n*n)*sizeof(double));
        double *b = (double *) malloc(n*sizeof(double));
        double *c = (double *) malloc(n*sizeof(double));


        for(int i = 0; i<(n*n); i++)
        {
            *(A+i) = ((double) rand() /RAND_MAX) * 2.0 - 1.0;//here 1.0 is maximum value in the range of random numbers
            // it is divided by RAND_MAX which is a constant predefined in the stdlib
        }

        for(int i = 0; i<n; i++)
        {
            *(b+i) = 1.0;
            *(c+i) = 0;
        }

        start_time = clock();
        mvmul(A,b,c,(size_t)n);
        end_time = clock();

        printf("%f ",c[n-1]);
        printf("\n");

        time_elapsed = ((double) (end_time - start_time)) / CLOCKS_PER_SEC * 1000; // time in ms
        printf("%f ",time_elapsed);

        free(A);
        free(b);
        free(c);
    }
}
