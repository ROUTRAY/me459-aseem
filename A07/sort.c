#include <stdlib.h>
#include <stdio.h>

#include "sort.h"

void sort(int* A, size_t n_elements)
{
    int noe = (int)(n_elements);//there is a risk of losing data in case n_elements has a very high value
    //code for insertion sort adapted from geeksforgeeks

    int i, key, j;

    for (i = 1; i < noe; i++)
    {
        key = *(A + i);
        j = i - 1;
        while (j >= 0 && *(A + j) > key)
        {
            *(A + j + 1) = *(A + j);
            j = j - 1;
        }
        *(A + j + 1) = key;
    }
}
