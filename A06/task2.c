#include <stdio.h>
#include <stdlib.h>
#include "structs.h"

int main()
{
    printf("The size of A is %lu bytes \n",sizeof(struct A));// %lu is the format specifier for long unsigned int, (size_t value)
    printf("The size of B is %lu bytes \n",sizeof(struct B));

    struct A *ptr = (struct A *)malloc(1*sizeof(struct A)); // allocates one struct memory in heap

    ptr->c = 'A';
    ptr->d = 3.1416;
    ptr->i = 1;

    printf("%d\n",ptr->i);
    printf("%f\n",ptr->d);
    printf("%c\n",ptr->c);

    free(ptr);
    return 0;
}
