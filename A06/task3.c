//code adapted from stackoverflow and lecture slides

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <math.h>

//comparing function
int comparefunc(const void *a, const void *b)
{
    return (*(int*)b - *(int*)a);
}

int main(int argc, char *argv[])
{
    if(argc==2)
    {
        for (char *k = argv[1]; *k!='\0'; ++k)
        {
            if(!(isdigit(*k)))
            {
                //printf("%c ", *k);
                exit(0);
            }
        }


        int N = atoi(argv[1]); // converts char value to int value
        if(N<=0)
        {
            //printf("ERROR: Enter a Positive Integer");
            exit(0);
        }
        else
        {
            int *arr = (int *) malloc((N+1)*sizeof(int));

            for(int i = 0; i<N+1; i++)
            {
                arr[i]=i;

                //print array in a single line with spaces
                printf("%d ",arr[i]);

            }
            printf("\n");

            qsort(arr, N+1, sizeof(int), (const void*)comparefunc);

            //print sorted array in a single line with spaces

            for(int i = 0; i<N+1; i++)
            {
                printf("%d ",arr[i]);

            }

            free(arr);
        }
    }
    else
    {
        printf("Insufficient or too many command line arguments");
        exit(0);
    }

    return 0;
}

