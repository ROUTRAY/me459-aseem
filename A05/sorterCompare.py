# -*- coding: utf-8 -*-
"""
Created on Sat Feb 25 19:18:13 2023

@author: aseem

Please note that the sorter function uses insertion sort whereas list.sort()
and numpy.sort() uses quick sort technique by default
"""

from pySort import sorter
import time
import argparse

import matplotlib.pyplot as plt
import numpy as np
import sys
import random
import math
import copy

parser = argparse.ArgumentParser()
parser.add_argument("plotOption", type=int, choices=[0, 1], help="Select 0 or 1 to sepcify plot or no plot")
    
args = parser.parse_args()
plotBool = False

if args.plotOption == 1:
    plotBool = True
    
X_data = [0]*5
timedata_sorter = [0]*5
timedata_sorted = [0]*5
timedata_npsort = [0]*5
    
for i in range(10,15):

    N = int(math.pow(2, i))
    #X_data[i-10] = math.log2(N)
    X_data[i-10] = N
    A_list = [random.randint(-10,10) for i in range(N)]
    #print(A_list)
    B_list = copy.deepcopy(A_list)
    #print(B_list)
    A = np.array(A_list)
    #print(A)
    #B = np.array(B_list)
    
    tic = time.perf_counter()  # start watch    
    sorter(A_list)
    #print(A_list)        
    toc = time.perf_counter()  # stop watch
    #print(f"Elapsed time with sorter: {(toc - tic)*1000:0.2f} ms with perf_counter for N =", N)
    timedata_sorter[i-10]=(toc-tic)*1000 # in milliseconds
    
    tic = time.perf_counter()  # start watch    
    B_list.sort() # list.sort() was used instead of sorted() as
    #print(B_list)       
    toc = time.perf_counter()  # stop watch
    #print(f"Elapsed time with 2-D lists: {(toc - tic)*1000:0.2f} ms with perf_counter for N =", N)
    timedata_sorted[i-10]=(toc-tic)*1000 # in milliseconds
    
    tic = time.perf_counter()  # start watch    
    A = np.sort(A) # the assignment was done as np.sort() does not change A.
    #print(A)        
    toc = time.perf_counter()  # stop watch
    #print(f"Elapsed time with 2-D lists: {(toc - tic)*1000:0.2f} ms with perf_counter for N =", N)
    timedata_npsort[i-10]=(toc-tic)*1000 # in milliseconds

print('\n')
print(A_list[0])
print('%.2f' % timedata_sorter[-1])
print(B_list[0])
print('%.2f' % timedata_sorted[-1])
print(A[0])
print('%.2f' % timedata_npsort[-1])

timedata_sorter_log = [math.log(timedata_sorter[i]) for i in range(5)]
timedata_sorted_log = [math.log(timedata_sorted[i]) for i in range(5)]
timedata_npsort_log = [math.log(timedata_npsort[i]) for i in range(5)]
    

if plotBool:
    
    fig,ax = plt.subplots()
    ax.set_xscale('log', base=2)
    ax.set_yscale('log', base=10)
    
    ax.plot(X_data,timedata_sorter,label='sorter function')
    ax.scatter(X_data,timedata_sorter)
    
    ax.plot(X_data,timedata_sorted,label = 'list.sort()')
    ax.scatter(X_data,timedata_sorted)
    
    ax.plot(X_data,timedata_npsort,label = 'Numpy sort')
    ax.scatter(X_data,timedata_npsort)
    
    plt.ylabel('Log[Execution Time in ms]')
    plt.xlabel('Log2[Matrix Size]  or  log2(#N)')
    plt.title('Execution Time v/s List Size')
    plt.legend(loc = 'upper left')
    plt.show()
