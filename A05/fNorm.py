# -*- coding: utf-8 -*-
"""
Created on Sat Feb 25 19:18:26 2023

@author: aseem

adapted from:
https://www.geeksforgeeks.org/how-to-read-csv-files-with-numpy/

"""

import numpy as np
import argparse

parser = argparse.ArgumentParser()
parser.add_argument("csv_name", type = str, help="Enter the filename of the csv file (without extension)")

args = parser.parse_args()
arr = np.genfromtxt(args.csv_name+".csv", delimiter=",", dtype=str)

#arr = np.array([[0,2,3],[1,4,5],[5,6,7]]) # sanity check
frob_norm = np.linalg.norm(arr)
print('%.3f' %frob_norm)
