# -*- coding: utf-8 -*-
"""
Created on Sat Feb 25 19:17:45 2023

@author: aseem
"""

from pyMatmul import matmul
import time
import argparse

import matplotlib.pyplot as plt
import numpy as np
import sys
import random
import math


parser = argparse.ArgumentParser()
parser.add_argument("plotOption", type=int, choices=[0, 1], help="Select 0 or 1 to sepcify plot or no plot")

args = parser.parse_args()
plotBool = False

if args.plotOption == 1:
    plotBool = True

X_data = [0]*4
timedata_list = [0]*4
timedata_numpy = [0]*4

for i in range(5,9):

    N = int(math.pow(2, i))
    X_data[i-5] = N
    #X_data[i-5] = math.log2(N)
    A_list = [[round(random.uniform(-1,1),4) for j in range(N)] for k in range(N)]
    B_list = [[round(random.uniform(-1,1),4) for j in range(N)] for k in range(N)]
    
    A = np.array(A_list)
    B = np.array(B_list)
    
    tic = time.perf_counter()  # start watch
    C = matmul(A_list,B_list)
    toc = time.perf_counter()  # stop watch
    #print(f"Elapsed time with 2-D lists: {(toc - tic)*1000:0.2f} ms with perf_counter for N =", N)
    timedata_list[i-5]=(toc-tic)*1000
    
    tic = time.perf_counter()  # start watch
    C = A@B
    toc = time.perf_counter()  # stop watch
    #print(f"Elapsed time with numpy array: {(toc - tic)*1000:0.2f} ms with perf_counter for N =", N)
    timedata_numpy[i-5]=(toc-tic)*1000
    #print('\n')

print('\n')
print(A_list[0][0])
print('%.2f' % timedata_list[-1])
print(A[0][0])
print('%.2f' % timedata_numpy[-1])

timedata_list_log = [math.log(timedata_list[i]) for i in range(4)]
timedata_numpy_log = [math.log(timedata_numpy[i]) for i in range(4)]

if plotBool:
    
    fig,ax = plt.subplots()
    
    # ax.plot(X_data,timedata_list_log,label='Normal List')
    # ax.scatter(X_data,timedata_list_log)
    
    # ax.plot(X_data,timedata_numpy_log,label = 'Numpy array')
    # ax.scatter(X_data,timedata_numpy_log)
    
    ax.plot(X_data,timedata_list,label='Normal List')
    ax.scatter(X_data,timedata_list)
    
    ax.plot(X_data,timedata_numpy,label = 'Numpy array')
    ax.scatter(X_data,timedata_numpy)
    
    ax.set_xscale('log', base=2)
    ax.set_yscale('log', base=10)
    
    plt.ylabel('Log[Execution Time (in ms)]')
    plt.xlabel('Log2[Matrix Size]  or  log2(#N)')
    plt.title('Execution Time v/s Size of Matrix')
    plt.legend(loc = 'upper left')
    plt.show()
