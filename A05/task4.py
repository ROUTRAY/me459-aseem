# -*- coding: utf-8 -*-
"""
Created on Thu Mar  2 12:16:36 2023

@author: aseem

code adapted from example code from EPD 455
"""

class human:
    """"A class representing a human"""
    def __init__(self, n, a):
        self.name = n
        self.age = a

    def __str__(self) :
        dummy = self.name
        dummy += ", age "
        dummy += '%s' % self.age
        dummy +=  "."
        return dummy

    def __add__ (self,nyears):
        self.age += nyears

    def __lt__(self,dummy):
        return self.age < dummy.age
    
    def __gt__(self,dummy):
        return self.age > dummy.age
    
    def __ge__(self,dummy):
        return self.age >= dummy.age

if __name__ == '__main__':

    bibi = human('Bibi',55)
    mary = human('Mary',39)
    mimi = human('Mimi',64)
    print(mary.name + " is younger than " + bibi.name + ": ")
    print(str(mary))
    print(str(mary<bibi))
    print(str(mimi>bibi))
    print(str(mary>=mimi))
