# -*- coding: utf-8 -*-
"""
Created on Tue Feb 21 11:10:40 2023

@author: aseem
"""

"""

code adapted from 

https://www.geeksforgeeks.org/python-program-multiply-two-matrices/

"""

def matmul(A,B):
    
    n = len(A) # assuming only N X N matrices are passed as arguments
    #print(n)
    C = [[0 for i in range(n)] for j in range(n)]
    #print(C)
    for i in range(len(A)):
        
        for j in range(len(B[0])):
            
            for k in range(len(B)):
                C[i][j] += A[i][k]*B[k][j]
                #print(C[i][j])

    return C


if __name__ == '__main__':
    
    A =[[1,0],[0,1]]
    B =[[1,0],[0,1]]

    # A =[[1,2],[3,4]]
    # B =[[1,2],[3,4]]
    
    C = matmul(A,B)
    print(C)
    
